package shoppingCartApp.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;
import shoppingCartApp.db.DbOperations;
import shoppingCartApp.exceptions.ProductNotFoundException;
import shoppingCartApp.product.Product;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class CartController implements Initializable {
    // payment anchorpane
    public Label ronLabel;
    public Label customerRestLabel;
    public Label restLabel;
    public JFXTextField customerField;
    public Label customerLabel;
    public JFXButton clearMoney;
    public JFXButton finishPayment;
    public JFXButton calculateRest;
    public AnchorPane paymentAnchor;
    public GridPane gridPadePayment;
    public GridPane gridPaneEAN;

    // finish payment anchorcart
    ObservableList<Product> observableListOfProducts = FXCollections.observableArrayList();
    @FXML
    private TableView<Product> tableView = new TableView<>(observableListOfProducts);
    @FXML
    private TableColumn<Product, String> tableColumnProductName;
    @FXML
    private TableColumn<Product, Double> tableColumnProductPrice;
    @FXML
    private JFXButton placeOrderBtn;
    @FXML
    private JFXButton cancelOrderBtn;
    @FXML
    private JFXButton unu, doi, trei, patru, cinci, sase, sapte, opt, noua, zero;
    @FXML
    private JFXButton unu1, doi1, trei1, patru1, cinci1, sase1, sapte1, opt1, noua1, zero1;
    @FXML
    private JFXButton findProductBtn;
    @FXML
    private JFXButton clearBtn;
    @FXML
    private JFXButton clearBtn1;
    @FXML
    private Label totalPriceLabel;
    @FXML
    private JFXTextField productEanTextField;
    public Label productEanLabel;
    @FXML
    private JFXTextField searchProductEan;
    private BigDecimal price = BigDecimal.ZERO;
    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    private LocalDateTime now = LocalDateTime.now();
    int maxLength = 13;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // textfiled allow only numbers
        searchProductEan.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    searchProductEan.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        // textfiled max length = 13 char
        searchProductEan.setOnKeyTyped(t -> {
            if (searchProductEan.getText().length() > maxLength) {
                int pos = searchProductEan.getCaretPosition();
                searchProductEan.setText(searchProductEan.getText(0, maxLength));
                searchProductEan.positionCaret(pos);
            }
        });
        disableBtns();
        ronLabel.setVisible(false);
        customerRestLabel.setVisible(false);
        restLabel.setVisible(false);
        customerField.setVisible(false);
        customerLabel.setVisible(false);
        clearMoney.setVisible(false);
        finishPayment.setVisible(false);
        calculateRest.setVisible(false);
        paymentAnchor.setVisible(false);

        paymentBtnsVisibleFalse();

        eanPandeVisibleFalse(true);
    }

    private void paymentBtnsVisibleFalse() {
        unu1.setVisible(false);
        doi1.setVisible(false);
        trei1.setVisible(false);
        patru1.setVisible(false);
        cinci1.setVisible(false);
        sase1.setVisible(false);
        sapte1.setVisible(false);
        opt1.setVisible(false);
        noua1.setVisible(false);
        zero1.setVisible(false);
        clearBtn1.setVisible(false);
        gridPadePayment.setVisible(false);
    }


/*
    // Metoda pentru generarea platii
    public void goToPayment(ActionEvent actionEvent) {
        if (observableListOfProducts != null) {
            try {
                Long receiptNumber = DbOperations.getReceiptNumber();
                Rectangle receiptSize = new Rectangle(400, 720);
                Document pdfTicket = new Document(receiptSize);
                PdfWriter.getInstance(pdfTicket,
                        new FileOutputStream(System.getProperty("user.home") + System.getProperty("file.separator") +
                                "Desktop" + System.getProperty("file.separator") + receiptNumber + ".pdf"));
                pdfTicket.open();
                String companyName = "Supermarket Braila";
                pdfTicket.add(new Paragraph(companyName, FontFactory.getFont(FontFactory.TIMES_BOLD, 24, Font.BOLD, BaseColor.BLACK)));
                pdfTicket.add(new Paragraph("Receipt number : " + receiptNumber, FontFactory.getFont(FontFactory.TIMES_BOLD, 15, BaseColor.BLACK)));
                pdfTicket.add(new Paragraph("Created receipt time : " + dtf.format(now), FontFactory.getFont(FontFactory.TIMES_BOLD, 15, BaseColor.BLACK)));
                pdfTicket.add(new Paragraph("---------------------------------------------------------------------------------------------",
                        FontFactory.getFont(FontFactory.TIMES_BOLD, 8, BaseColor.BLACK)));
                for (Product productFromObservableList : observableListOfProducts) {
                    pdfTicket.add(new Paragraph(productFromObservableList.getName() + " : " + "                                                     "
                            + productFromObservableList.getPrice() + " RON", FontFactory.getFont(FontFactory.TIMES_BOLD, 10, BaseColor.BLACK)));
                }
                pdfTicket.add(new Paragraph("---------------------------------------------------------------------------------------------",
                        FontFactory.getFont(FontFactory.TIMES_BOLD, 8, BaseColor.BLACK)));
                pdfTicket.add(new Paragraph("Total : " + price + " RON", FontFactory.getFont(FontFactory.TIMES_BOLD, 10, BaseColor.BLACK)));
                pdfTicket.close();
                DbOperations.newReceipt(receiptNumber, price, LocalDate.now());
                DbOperations.updateStocks(observableListOfProducts);
            } catch (DocumentException | FileNotFoundException e) {
                e.printStackTrace();
            }
            assert observableListOfProducts != null;
            observableListOfProducts.clear();
            tableView.getItems().clear();
            price = BigDecimal.ZERO;
            totalPriceLabel.setText("");
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Your cart it's empty");
            alert.showAndWait();
        }
    }

 */


    public void goToPayment(ActionEvent actionEvent) {
        if (observableListOfProducts.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Your cart it's empty");
            alert.showAndWait();
        } else {
            ronLabel.setVisible(true);
            customerRestLabel.setVisible(true);
            restLabel.setVisible(true);
            customerField.setVisible(true);
            customerLabel.setVisible(true);
            clearMoney.setVisible(true);
            finishPayment.setVisible(true);
            calculateRest.setVisible(true);
            paymentAnchor.setVisible(true);
            tableView.setVisible(false);
            productEanTextField.setVisible(false);
            productEanLabel.setVisible(false);
            //
            eanPandeVisibleFalse(false);

            gridPanePaymentVisibleTrue(gridPadePayment, true, unu1, doi1, trei1, patru1, cinci1, sase1, sapte1, opt1, noua1, zero1, clearBtn1);

        }

    }

    private void gridPanePaymentVisibleTrue(GridPane gridPadePayment, boolean b, JFXButton unu1, JFXButton doi1, JFXButton trei1, JFXButton patru1, JFXButton cinci1, JFXButton sase1, JFXButton sapte1, JFXButton opt1, JFXButton noua1, JFXButton zero1, JFXButton clearBtn1) {
        gridPadePayment.setVisible(b);
        unu1.setVisible(b);
        doi1.setVisible(b);
        trei1.setVisible(b);
        patru1.setVisible(b);
        cinci1.setVisible(b);
        sase1.setVisible(b);
        sapte1.setVisible(b);
        opt1.setVisible(b);
        noua1.setVisible(b);
        zero1.setVisible(b);
        clearBtn1.setVisible(b);
    }

    private void eanPandeVisibleFalse(boolean b) {
        gridPanePaymentVisibleTrue(gridPaneEAN, b, unu, doi, trei, patru, cinci, sase, sapte, opt, noua, zero, findProductBtn);
        clearBtn.setVisible(b);
    }


    // incarcarea Produsului cand e scanat codul ean intr-un tableview , nume si pret
    public void loadColumns() {
        tableColumnProductName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableColumnProductPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
    }

    // functia keyevent pusa pe textfield pentru a cauta in db produsul si al insera in tableview , in cart ,
// a calcula pretul total al produselor adaugate in cos
    public void textFieldKeyTyped(KeyEvent keyEvent) throws ProductNotFoundException {
        Product productFromDb = DbOperations.getProdusByCode(productEanTextField.getText());
        if (productFromDb != null) {
            observableListOfProducts.add(productFromDb);
            System.out.println("My Produs " + productFromDb);
            System.out.println("My observaleList " + observableListOfProducts);
            tableView.setItems(observableListOfProducts);
            loadColumns();
            double pret = productFromDb.getPrice();
            price = price.add(BigDecimal.valueOf(pret));
            totalPriceLabel.setVisible(true);
            totalPriceLabel.setText(String.valueOf(price));
            productEanTextField.clear();
            if (observableListOfProducts.size() > 1) {
                String title = "Un nou produs a fost adaugat in cos! ";
                String message = productFromDb.getName() + " a fost adaugat in cos";
                TrayNotification tray = new TrayNotification();
                AnimationType type = AnimationType.POPUP;
                tray.setAnimationType(type);
                tray.setTitle(title);
                tray.setMessage(message);
                tray.setNotificationType(NotificationType.SUCCESS);
                tray.showAndDismiss(Duration.millis(1000));
            }
        }
    }

    public void calculateRest(ActionEvent keyEvent) {
        BigDecimal rest = BigDecimal.ZERO;
        if (!customerField.getText().isEmpty()) {
            BigDecimal customer = new BigDecimal(customerField.getText());
            BigDecimal total = new BigDecimal(totalPriceLabel.getText());
            rest = customer.subtract(total);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "You need to insert a value!");
            alert.showAndWait();
        }
        customerRestLabel.setText(String.valueOf(rest));
    }

    public void clearMoney(ActionEvent actionEvent) {
        customerField.clear();
        customerRestLabel.setText("");
    }

    // butonul cu functia de a goli cosul , tablewview , lista ordonata
    public void cancelOrder(ActionEvent actionEvent) {
        observableListOfProducts.clear();
        tableView.getItems().clear();
        price = BigDecimal.ZERO;
        totalPriceLabel.setText("");
    }

    public void enableButtons(MouseEvent mouseEvent) {
        unu.setDisable(false);
        doi.setDisable(false);
        trei.setDisable(false);
        patru.setDisable(false);
        cinci.setDisable(false);
        sase.setDisable(false);
        sapte.setDisable(false);
        opt.setDisable(false);
        noua.setDisable(false);
        zero.setDisable(false);
        findProductBtn.setDisable(false);
        clearBtn.setDisable(false);

    }

    private void disableBtns() {
        unu.setDisable(true);
        doi.setDisable(true);
        trei.setDisable(true);
        patru.setDisable(true);
        cinci.setDisable(true);
        sase.setDisable(true);
        sapte.setDisable(true);
        opt.setDisable(true);
        noua.setDisable(true);
        zero.setDisable(true);
        findProductBtn.setDisable(true);
        clearBtn.setDisable(true);
    }

    public void findProductBtnMouseEvent(MouseEvent mouseEvent) throws ProductNotFoundException {
        if (searchProductEan.getText().isEmpty() || searchProductEan.getText().length() != maxLength) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "EAN Code is short or missing!");
            alert.showAndWait();
        } else {
            Product productFromDb = DbOperations.getProdusByCode(searchProductEan.getText());
            if (productFromDb != null) {
                observableListOfProducts.add(productFromDb);
                System.out.println("My Produs " + productFromDb);
                System.out.println("My observaleList " + observableListOfProducts);
                tableView.setItems(observableListOfProducts);
                loadColumns();
                double pret = productFromDb.getPrice();
                price = price.add(BigDecimal.valueOf(pret));
                totalPriceLabel.setVisible(true);
                totalPriceLabel.setText(price + " RON");
                productEanTextField.clear();
                if (observableListOfProducts.size() > 1) {
                    String title = "Informatii produse! ";
                    String message = productFromDb.getName() + "a fost gasit si a fost adaugat in cos";
                    TrayNotification tray = new TrayNotification();
                    AnimationType type = AnimationType.POPUP;
                    tray.setAnimationType(type);
                    tray.setTitle(title);
                    tray.setMessage(message);
                    tray.setNotificationType(NotificationType.SUCCESS);
                    tray.showAndDismiss(Duration.millis(1000));
                }
            }
        }
        searchProductEan.clear();
        disableBtns();
    }

    public void clearAction(ActionEvent actionEvent) {
        searchProductEan.clear();
    }

    public void clearAction1(ActionEvent actionEvent) {
        customerField.clear();
    }

    public void handleButtonAction(ActionEvent event) {
        if (event.getSource() == unu) {
            searchProductEan.setText(searchProductEan.getText() + unu.getText());
        } else if (event.getSource() == doi) {
            searchProductEan.setText(searchProductEan.getText() + doi.getText());
        } else if (event.getSource() == trei) {
            searchProductEan.setText(searchProductEan.getText() + trei.getText());
        } else if (event.getSource() == patru) {
            searchProductEan.setText(searchProductEan.getText() + patru.getText());
        } else if (event.getSource() == cinci) {
            searchProductEan.setText(searchProductEan.getText() + cinci.getText());
        } else if (event.getSource() == sase) {
            searchProductEan.setText(searchProductEan.getText() + sase.getText());
        } else if (event.getSource() == sapte) {
            searchProductEan.setText(searchProductEan.getText() + sapte.getText());
        } else if (event.getSource() == opt) {
            searchProductEan.setText(searchProductEan.getText() + opt.getText());
        } else if (event.getSource() == noua) {
            searchProductEan.setText(searchProductEan.getText() + noua.getText());
        } else if (event.getSource() == zero) {
            searchProductEan.setText(searchProductEan.getText() + zero.getText());
        }
    }

    public void finishPayment(ActionEvent actionEvent) {
        if (observableListOfProducts != null) {
            try {
                Long receiptNumber = DbOperations.getReceiptNumber();
                Rectangle receiptSize = new Rectangle(400, 720);
                Document pdfTicket = new Document(receiptSize);
                PdfWriter.getInstance(pdfTicket,
                        new FileOutputStream(System.getProperty("user.home") + System.getProperty("file.separator") +
                                "Desktop" + System.getProperty("file.separator") + receiptNumber + ".pdf"));
                pdfTicket.open();
                String companyName = "Supermarket Braila";
                pdfTicket.add(new Paragraph(companyName, FontFactory.getFont(FontFactory.TIMES_BOLD, 24, Font.BOLD, BaseColor.BLACK)));
                pdfTicket.add(new Paragraph("Receipt number : " + receiptNumber, FontFactory.getFont(FontFactory.TIMES_BOLD, 15, BaseColor.BLACK)));
                pdfTicket.add(new Paragraph("Created receipt time : " + dtf.format(now), FontFactory.getFont(FontFactory.TIMES_BOLD, 15, BaseColor.BLACK)));
                pdfTicket.add(new Paragraph("---------------------------------------------------------------------------------------------",
                        FontFactory.getFont(FontFactory.TIMES_BOLD, 8, BaseColor.BLACK)));
                for (Product productFromObservableList : observableListOfProducts) {
                    pdfTicket.add(new Paragraph(productFromObservableList.getName() + " : " + " BUC x 1" + "                                                     "
                            + productFromObservableList.getPrice() + " RON", FontFactory.getFont(FontFactory.TIMES_BOLD, 10, BaseColor.BLACK)));
                }
                pdfTicket.add(new Paragraph("---------------------------------------------------------------------------------------------",
                        FontFactory.getFont(FontFactory.TIMES_BOLD, 8, BaseColor.BLACK)));
                pdfTicket.add(new Paragraph("Total products : " + observableListOfProducts.size()));
                pdfTicket.add(new Paragraph("Total : " + price + " RON", FontFactory.getFont(FontFactory.TIMES_BOLD, 10, BaseColor.BLACK)));
                pdfTicket.close();
                DbOperations.newReceipt(receiptNumber, price, LocalDate.now());
                DbOperations.updateStocks(observableListOfProducts);
            } catch (DocumentException | FileNotFoundException e) {
                e.printStackTrace();
            }
            assert observableListOfProducts != null;
            price = BigDecimal.ZERO;
            totalPriceLabel.setText("");
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Your cart it's empty");
            alert.showAndWait();
        }
        ronLabel.setVisible(false);
        customerRestLabel.setVisible(false);
        restLabel.setVisible(false);
        customerField.setVisible(false);
        customerLabel.setVisible(false);
        clearMoney.setVisible(false);
        finishPayment.setVisible(false);
        calculateRest.setVisible(false);
        paymentAnchor.setVisible(false);
        tableView.setVisible(true);
        productEanTextField.setVisible(true);
        productEanLabel.setVisible(true);
        observableListOfProducts.clear();
        tableView.getItems().clear();
        paymentBtnsVisibleFalse();
        eanPandeVisibleFalse(true);

    }

    public void handleButtonAction1(MouseEvent actionEvent) {
        if (actionEvent.getSource() == unu1) {
            customerField.setText(customerField.getText() + unu1.getText());
        } else if (actionEvent.getSource() == doi1) {
            customerField.setText(customerField.getText() + doi1.getText());
        } else if (actionEvent.getSource() == trei1) {
            customerField.setText(customerField.getText() + trei1.getText());
        } else if (actionEvent.getSource() == patru1) {
            customerField.setText(customerField.getText() + patru1.getText());
        } else if (actionEvent.getSource() == cinci1) {
            customerField.setText(customerField.getText() + cinci1.getText());
        } else if (actionEvent.getSource() == sase1) {
            customerField.setText(customerField.getText() + sase1.getText());
        } else if (actionEvent.getSource() == sapte1) {
            customerField.setText(customerField.getText() + sapte1.getText());
        } else if (actionEvent.getSource() == opt1) {
            customerField.setText(customerField.getText() + opt1.getText());
        } else if (actionEvent.getSource() == noua1) {
            customerField.setText(customerField.getText() + noua1.getText());
        } else if (actionEvent.getSource() == zero1) {
            customerField.setText(customerField.getText() + zero1.getText());
        }
    }
}

