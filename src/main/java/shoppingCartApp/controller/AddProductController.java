package shoppingCartApp.controller;

import com.jfoenix.controls.JFXButton;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import shoppingCartApp.db.DbOperations;

import java.net.URL;
import java.util.ResourceBundle;

public class AddProductController implements Initializable {
    @FXML
    private TextField eanCodeTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField priceTextField;
    @FXML
    private TextField stockTextField;
    @FXML
    private JFXButton addBtn;
    @FXML
    private JFXButton backBtn;

    public void addProduct(ActionEvent actionEvent) {

        if (!nameTextField.getText().isEmpty() && !eanCodeTextField.getText().isEmpty()
                && !priceTextField.getText().isEmpty() && !stockTextField.getText().isEmpty()) {
            DbOperations.newProduct(nameTextField.getText(), eanCodeTextField.getText(), Double.parseDouble(priceTextField.getText()), Integer.parseInt(stockTextField.getText()));
            nameTextField.clear();
            eanCodeTextField.clear();
            priceTextField.clear();
            stockTextField.clear();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Field missing");
            alert.showAndWait();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        priceTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    priceTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        stockTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    stockTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        eanCodeTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    eanCodeTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        eanCodeTextField.setOnKeyTyped(t -> {
            int maxLength = 12;
            if (eanCodeTextField.getText().length() > maxLength) {
                int pos = eanCodeTextField.getCaretPosition();
                eanCodeTextField.setText(eanCodeTextField.getText(0, maxLength));
                eanCodeTextField.positionCaret(pos);
            }
        });
    }

    public void backAction(ActionEvent actionEvent) {
        backBtn.getScene().getWindow().hide();
    }
}
