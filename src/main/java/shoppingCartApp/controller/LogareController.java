package shoppingCartApp.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogareController implements Initializable {
    public JFXTextField username;
    public JFXPasswordField password;
    public JFXButton loginBtn;
    public JFXButton closeBtn;
    public MenuItem addProductBtn;
    public SplitMenuButton splitMenuButton;

    String user = "1234";
    String pass = "1234";

    public void logare(ActionEvent actionEvent) {
        if (user.isEmpty() || pass.isEmpty()) {
            String title = "Username Or Password problem!";
            String message = username.getText().isEmpty() + " " + password.getText().isEmpty();
            TrayNotification tray = new TrayNotification();
            AnimationType type = AnimationType.POPUP;
            tray.setAnimationType(type);
            tray.setTitle(title);
            tray.setMessage(message);
            tray.setNotificationType(NotificationType.ERROR);
            tray.showAndDismiss(Duration.millis(3000));
        } else {
            if (user.equals(username.getText()) && pass.equals(password.getText())) {
                String title = "LogIn Success!";
                String message = username.getText() + " Logged In!";
                TrayNotification tray = new TrayNotification();
                AnimationType type = AnimationType.POPUP;
                tray.setAnimationType(type);
                tray.setTitle(title);
                tray.setMessage(message);
                tray.setNotificationType(NotificationType.SUCCESS);
                tray.showAndDismiss(Duration.millis(3000));
                loginBtn.setOnMouseClicked((event) -> {
                    try {
                        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Shop.fxml"));
                        Scene scene = new Scene(root);
                        Stage stage = new Stage();
                        stage.setTitle("Shopping Mall Braila");
                        stage.setScene(scene);
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.show();
                    } catch (IOException e) {
                        Logger logger = Logger.getLogger(getClass().getName());
                        logger.log(Level.SEVERE, "Failed to create new Window.", e);
                    }
                });
            } else {
                String title = "Username Or Password problem!";
                String message = "User";
                TrayNotification tray = new TrayNotification();
                AnimationType type = AnimationType.POPUP;
                tray.setAnimationType(type);
                tray.setTitle(title);
                tray.setMessage(message);
                tray.setNotificationType(NotificationType.ERROR);
                tray.showAndDismiss(Duration.millis(3000));
            }
        }

    }


    public void loadRegisterScene(ActionEvent mouseEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/AddProduct.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Add Product to SuperMarket");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        username.setOnKeyTyped(t -> {
            int maxLength = 3;
            if (username.getText().length() > maxLength) {
                int pos = username.getCaretPosition();
                username.setText(username.getText(0, maxLength));
                username.positionCaret(pos);
            }
        });
        password.setOnKeyTyped(t -> {
            int maxLength = 3;
            if (password.getText().length() > maxLength) {
                int pos = password.getCaretPosition();
                password.setText(password.getText(0, maxLength));
                password.positionCaret(pos);
            }
        });
        username.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    username.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        password.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    password.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }

    public void close(ActionEvent actionEvent) {
        System.exit(0);
    }
}
