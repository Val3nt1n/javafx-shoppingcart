package shoppingCartApp.exceptions;

public class PaymentException extends Exception {
    private String message;
    public PaymentException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
