package shoppingCartApp.exceptions;

public class ProductNotFoundException extends Exception {

    private String message;
    public ProductNotFoundException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
