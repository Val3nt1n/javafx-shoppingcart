package shoppingCartApp.exceptions;

public class ReceiptNumberNotFoundException extends Exception {
    private String message;
    public ReceiptNumberNotFoundException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
