package shoppingCartApp.db;

import javafx.util.Duration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import shoppingCartApp.exceptions.ProductNotFoundException;
import shoppingCartApp.product.Product;
import shoppingCartApp.receipts.Receipt;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class DbOperations {
    public final static SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

    DbOperations() {
    }

    // Metoda de a scoate din db o lista cu produse
    public static List<Product> getProducts() {
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            Query<Product> result = session.createQuery("from Product", Product.class);
            return result.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }


    // Metoda care scoate din db un produs in functie de codul ean
    public static Product getProdusByCode(String productsByCode) throws ProductNotFoundException {
        assert DbOperations.sessionFactory != null;
        try (Session session = DbOperations.sessionFactory.openSession()) {
            Query<Product> queryProducts = session.createNamedQuery("cautaProdus", Product.class);
            queryProducts.setParameter("productByCode", productsByCode);
            List<Product> productList = queryProducts.list();
            if (productList.size() == 0) {
                String title = "Stoc insuficient!";
                String message = productsByCode + " does not exist!";
                TrayNotification tray = new TrayNotification();
                AnimationType type = AnimationType.SLIDE;
                tray.setAnimationType(type);
                tray.setTitle(title);
                tray.setMessage(message);
                tray.setNotificationType(NotificationType.WARNING);
                tray.showAndDismiss(Duration.millis(2000));
                throw new ProductNotFoundException("Produsul " + productsByCode + " does not exist!");
            }
            return productList.get(0);
        } catch (Exception e) {
            System.err.println("Something has happened..." + e);
            throw new ProductNotFoundException(e.getMessage());
        }
    }


    // Metoda care insereaza fiecare chitanta , cu un numar , un pret total al produselor si data cand sa eliberat chitanta
    public static void newReceipt(long receiptNumber, BigDecimal total, LocalDate date) {
        Transaction transaction = null;
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(new Receipt(receiptNumber, total, date));
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) transaction.rollback();
        }
    }

    public static void newProduct(String name, String code, double price, int stock) {
        Transaction transaction = null;
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(new Product(name,code,price,stock));
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) transaction.rollback();
        }

    }


    // Metoda care scoate un ticket din db , o folosesc pentru a nu insera acelasi ticket de 2 ori.
    public static Long getReceiptNumber() {
        assert sessionFactory != null;
        String sql = "SELECT MAX(receiptNumber) + 1 AS newReceiptNumber FROM Receipt";
        try (Session session = sessionFactory.openSession()) {
            Query<Long> result = session.createQuery(sql);
            return result.list().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

/*
    public static void updateProducts(List<Product> products) {
        Transaction transaction = null;
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            final Query<Product> updateStmt = session.createNamedQuery("updateProduct", Product.class);
            int i = 0;
            for (Product product : products) {
                updateStmt.setParameter("newStock", product.getStock() - 1);
                updateStmt.setParameter("code", product.getCode());
                updateStmt.executeUpdate();
                if (++i % 20 == 0) {
                    session.flush();
                    session.clear();
                }
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) transaction.rollback();
        }
    }

 */

    private static Connection getConnection() {

        final String URL = "jdbc:mysql://localhost:3306/sale";
        final String driver = "com.mysql.cj.jdbc.Driver";
        final String username = "root";
        final String password = "Parola123";

        try {
            Class.forName(driver).newInstance();
            return DriverManager.getConnection(URL, username, password);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void updateStocks(List<Product> products) {
        String sql = "UPDATE produse SET stock = stock-1 WHERE code = ?";

        try (Connection con = getConnection()) {
            if (con != null) {
                try (PreparedStatement ps = con.prepareStatement(sql)) {
                    for (Product product : products) {
                        ps.setString(1, product.getCode());
                        ps.addBatch();
                    }
                    ps.executeBatch();
                }
            } else {
                System.err.println("Could not obtain DB connection.");
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }


}
