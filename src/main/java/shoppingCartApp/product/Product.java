package shoppingCartApp.product;


import javax.persistence.*;
import java.util.StringJoiner;

@Entity
@Table(name = "produse")
@NamedQuery(name = "cautaProdus", query = "from Product where code =:productByCode and stock > 0")
/*
@NamedQueries({
        @NamedQuery(name = "cautaProdus", query = "from Product where code =:productByCode and stock > 0"),
        @NamedQuery(name = "updateProduct", query = "UPDATE Product SET stock = :newStock WHERE code = :code")
})

 */
public class Product {

    @Id
    private String code;
    private String name;
    private double price;
    private int stock;

    public Product() {
    }

    public Product(String name, String code, double price, int stock) {

        this.name = name;
        this.code = code;
        this.price = price;
        this.stock = stock;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Product.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("code='" + code + "'")
                .add("price=" + price)
                .add("stock=" + stock)
                .toString();
    }
}
