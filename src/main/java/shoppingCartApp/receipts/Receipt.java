package shoppingCartApp.receipts;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "receipts")
public class Receipt {

    @Id
    private Long receiptNumber;
    private BigDecimal total = BigDecimal.ZERO;
    private LocalDate createdReceipt;


    public Receipt() {
    }

    public Receipt(Long receiptNumber, BigDecimal total, LocalDate createdReceipt) {
        this.receiptNumber = receiptNumber;
        this.total = total;
        this.createdReceipt = createdReceipt;
    }

    public Receipt(BigDecimal total, LocalDate createdReceipt) {
        this.total = total;
        this.createdReceipt = createdReceipt;
    }

    public Long getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(Long receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public LocalDate getCreatedReceipt() {
        return createdReceipt;
    }

    public void setCreatedReceipt(LocalDate createdReceipt) {
        this.createdReceipt = createdReceipt;
    }
}
